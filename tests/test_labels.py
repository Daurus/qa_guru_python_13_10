import allure
from allure_commons.types import Severity




def test_dynamic_labels():
    allure.dynamic.tag("web")
    allure.dynamic.severity(Severity.NORMAL)
    allure.dynamic.feature("Проверка ишью в репозитории")
    allure.dynamic.story("В списке есть ишью №76")
    allure.dynamic.description("Описание ишью")
    allure.dynamic.suite('Тестовый сьют')
    allure.dynamic.link("https://github.com", name="Testing")
    pass


@allure.tag("web")
@allure.severity(Severity.NORMAL)
@allure.label("owner", "eroshenkoam")
@allure.feature("Проверка ишью в репозитории")
@allure.story("Авторизованный пользователь может создать задачу в репозитории")
@allure.link("https://github.com", name="Testing")
def test_decorator_labels():
    pass
