import allure
from selene import browser, by, be


def test_search_issue():
    with allure.step("Открываем главную страницу github"):
        browser.open("")

    with allure.step("Ищем репозиторий"):
        browser.element('.search-input').click()
        browser.element('#query-builder-test').type('eroshenkoam/allure-example').submit()

    with allure.step("Переходим по ссылке репозитория"):
        browser.element(by.link_text('eroshenkoam/allure-example')).click()

    with allure.step("Открываем таб Issues"):
        browser.element('#issues-tab').click()

    with allure.step("Проверяем наличие Issue с номером 76"):
        browser.element(by.partial_text("#76")).should(be.visible)