import allure
from selene import browser, by, be


def test_github():
            browser.open("")

            browser.element('.search-input').click()

            browser.element('#query-builder-test').type('eroshenkoam/allure-example').submit()

            browser.element(by.link_text('eroshenkoam/allure-example')).click()
            browser.element('#issues-tab').click()

            browser.element(by.partial_text("#76")).should(be.visible)